$("#vehicle-list").mCustomScrollbar({
    theme: 'dark',
    mouseWheel: {
        deltaFactor: 50
    },
});

$(".shopping-tools-wrapper").mCustomScrollbar({
    theme: 'dark',
})

function move(value) {
    $("#vehicle-list").mCustomScrollbar("scrollTo", value, {
        scrollEasing: "easeOut"
    });
}

new WOW().init();

$(".vehicle-nav ul li a").click(function () {
    /* if ($(this).attr("id") === "cars-nav") {
        move("#cars");
    } */
    switch ($(this).attr("id")) {
        case "cars-nav":
            move("#cars");
            break;
        case "suv-nav":
            move("#suv");
            break;
        case "minivan-nav":
            move("#minivan");
            break;
        case "environmental-nav":
            move("#environmental");
            break;
        default:
            break;
    }
})

$("#navbar > a").click(function () {
    if(window.innerWidth >= 768) {
        $(this.children[0]).toggleClass("transform-rotate");
    }
})

let count = "";
$("#navbar > a").click(function () {
    if (count === "") { // Nhấn lần đầu
        count = $(this).attr("id"); // gán count bằng giá trị id của nút đang nhấn
        $(this).addClass("active"); // thêm class active

        if (count === "vehicle-link") {
            $("#vehicles").removeClass("hide-vehicle");
            $(".vehicle-nav").addClass("animate__fadeInDown");
            $("#vehicle-list").addClass("animate__fadeInUp");

        } else if (count === "shopping-tools-link") {
            $("#shopping-tools").removeClass("hide-vehicle");
            $("#shopping-tools-content").addClass("animate__fadeInDown");

        }
    } else {
        if (count === $(this).attr("id")) {//click lại nút đang đc click
            $(this).removeClass("active");
            count = "";
            $(".vehicle-nav").addClass("animate__fadeOutUp");
            $("#vehicle-list").addClass("animate__fadeOutDown");

            $("#shopping-tools-content").addClass("animate__fadeOutUp");

            $(".button-close").addClass("animate__fadeOut");

            setTimeout(function () {
                $("#vehicles").addClass("hide-vehicle");
                $(".vehicle-nav").removeClass("animate__fadeOutUp");
                $("#vehicle-list").removeClass("animate__fadeOutDown");

                $("#shopping-tools").addClass("hide-vehicle");
                $("#shopping-tools-content").removeClass("animate__fadeOutUp");

                $(".button-close").removeClass("animate__fadeOut");
            }, 300);


        } else { // đang click nút này thì click sang nút khác
            $("#navbar > a").removeClass("active");
            $(this).addClass("active");
            count = $(this).attr("id");

            if (count === "shopping-tools-link") { //shopping-tool
                $(".vehicle-nav").addClass("animate__fadeOutUp");
                $("#vehicle-list").addClass("animate__fadeOutDown");

                $("#shopping-tools-content").addClass("animate__fadeInDown");
                $(".button-close").addClass("animate__fadeOut");
                setTimeout(function () {
                    $("#vehicles").addClass("hide-vehicle");
                    $(".vehicle-nav").removeClass("animate__fadeOutUp");
                    $("#vehicle-list").removeClass("animate__fadeOutDown");

                    $("#shopping-tools").removeClass("hide-vehicle");
                    $(".button-close").removeClass("animate__fadeOut");

                }, 300);

            } else { // vehicle
                $(".vehicle-nav").addClass("animate__fadeInDown");
                $("#vehicle-list").addClass("animate__fadeInUp");

                $("#shopping-tools-content").addClass("animate__fadeOutUp");
                $(".button-close").addClass("animate__fadeIn");
                setTimeout(function () {
                    $("#shopping-tools").addClass("hide-vehicle");
                    $("#shopping-tools-content").removeClass("animate__fadeOutUp");

                    $("#vehicles").removeClass("hide-vehicle");
                }, 300);
            }
        }
    }
})

$(".button-close").click(function () {
    $(".vehicle-nav").addClass("animate__fadeOutUp");
    $("#vehicle-list").addClass("animate__fadeOutDown");

    $("#shopping-tools-content").addClass("animate__fadeOutUp");

    $(".button-close").addClass("animate__fadeOut");
    setTimeout(function () {
        $("#vehicles").addClass("hide-vehicle");
        $(".vehicle-nav").removeClass("animate__fadeOutUp");
        $("#vehicle-list").removeClass("animate__fadeOutDown");

        //
        $("#shopping-tools").addClass("hide-vehicle");
        $("#shopping-tools-content").removeClass("animate__fadeOutUp");

        $(".button-close").removeClass("animate__fadeOut");
    }, 300);
    $("#navbar a").removeClass("active");
    count = "";

    setTimeout(function () {
        $(".vehicle-nav ul li a").removeClass("active");
        $("#cars-nav").addClass("active");
    }, 300);
    
    console.log($(this).hasClass("btn-close-vehicles"));
    if($(this).hasClass("btn-close-vehicles") && window.innerWidth >= 768) {
        $("#vehicle-link i").toggleClass("transform-rotate")
    } else {
        $("#shopping-tools-link i").toggleClass("transform-rotate")
    }
})

$(".vehicle-product").addClass("animate__animated animate__fadeInUp");

$(".vehicle-nav ul li a").click(function () { // active vehicle-nav
    $(".vehicle-nav ul li a").removeClass("active");
    $(this).addClass("active");
})

const swiper = new Swiper('.swiper', {
    // Optional parameters
    centeredSlides: true,
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});

$(".carousel-indicators-config li").click(function () {
    $(".carousel-indicators-config li").removeClass("active");
    $(this).addClass("active");
})

// Xử lí indicators của our-vehicle
$(".indicators-xs-header").click(function () {
    $(".carousel-indicators-xs ol").slideToggle();

    if ($(".indicators-xs-header i").hasClass("transform-rotate")) {
        $(".indicators-xs-header i").removeClass("transform-rotate");
    } else {
        $(".indicators-xs-header i").addClass("transform-rotate");
    }
})

$(".carousel-indicators-xs ol li").click(function () {
    $(".indicators-title span").html($(this).html());
    $(".carousel-indicators-xs ol").slideUp();
})

let count_slide = 0;
$(".carousel-control-prev").click(function () {
    $(".carousel-indicators-config li").removeClass("active");
    if (count_slide == 0) {
        count_slide = 4;
    } else {
        count_slide--;
    }
    $(".carousel-indicators-config li").each(function (index) {
        if (index == count_slide) {
            $(this).addClass("active");
            $(".indicators-title span").html($(this).html());
        }
    })
})

$(".carousel-control-next").click(function () {
    $(".carousel-indicators-config li").removeClass("active");
    if (count_slide == 4) {
        count_slide = 0;
    } else {
        count_slide++;
    }
    $(".carousel-indicators-config li").each(function (index) {
        if (index == count_slide) {
            $(this).addClass("active");
            $(".indicators-title span").html($(this).html());
        }
    })
})

// Xử lí Back To Top
$(".back-to-top").click(function () {
    $("body, html").animate({ scrollTop: 0 }, 1000);
})

// Xử lí footer
function addCollapse() {
    let width = window.innerWidth;
    // console.log(width);
    if (width >= 992) {
        $(".collapse-ul").removeClass("collapse");
    } else {
        $(".collapse-ul").addClass("collapse");
        $(".footer-detail h3").click(function () {
            // console.log($(this.childNodes[3]));
            if ($(this.childNodes[3]).hasClass("fa-plus")) {
                $(this.childNodes[3]).removeClass("fa-plus");
                $(this.childNodes[3]).addClass("fa-minus");
            } else {
                $(this.childNodes[3]).removeClass("fa-minus");
                $(this.childNodes[3]).addClass("fa-plus");
            }
        })
    }
}

$(window).resize(function () {
    addCollapse();
})

$(document).ready(function () {
    addCollapse();
})

$(".js-btn-search").click(function(event) {
    $("#search-area").toggleClass("show");
    event.preventDefault();
})

/* Modal */
/* $(".btn-contact").click(function(event) {
    event.preventDefault();
    $(".modal-contact").addClass("open");
})
$(".modal-contact").click(function() {
    $(".modal-contact").removeClass("open");
})
$(".js-modal-close").click(function() {
    $(".modal-contact").removeClass("open");
})
$(".modal-container").click(function(event) {
    event.stopPropagation();
}) */